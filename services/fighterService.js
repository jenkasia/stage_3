const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    searchAll() {
        const item = FighterRepository.getAll();
        if (item.length == 0) {
            return null;
        }
        return item;
    }

    search(fighterId) {
        const item = FighterRepository.getOne({ id: fighterId });
        if (!item) {
            return null;
        }
        return item;

    }

    createFighter(data) {
        const existingChecker = !!FighterRepository.getOne({ name: data.name })
        if (existingChecker) {
            return null
        }
        else {
            const result = FighterRepository.create(data)
            return result
        }
    }

    updateFighter(fighterId, data) {
        const itemAvailibility = !!this.search(fighterId)
        if (itemAvailibility) {
            const item = FighterRepository.update(fighterId, data);
            return item;
        }
        else {
            return null;
        }
    }

    deleteFighter(fighterId) {
        const item = FighterRepository.delete(fighterId);
        if (!item) {
            return null;
        }
        return item[0];
    }
}

module.exports = new FighterService();