const { fighter } = require('../models/fighter');

const removeExcessFields = (body) => {
    for (let key in body) {
        if (!fighter.hasOwnProperty(key)) {
            delete body[key]
        }
    }
}

const checkForNeededKeys = (fighterInstance) => {
    for (let key in fighter) {
        if (!fighterInstance.hasOwnProperty(key) && key !== 'id') {
            return false
        }
    }
    return true
}

const createFighterValid = (req, res, next) => {
    const isAllFeilds = !checkForNeededKeys(req.body)

    if (isAllFeilds) {
        res
            .status(400)
            .send({
                error: true,
                message: `There are miss some fields in request body`
            });
    }
    else if (req.body.health >= 100) {
        res
            .status(400)
            .send({
                error: true,
                message: `Fighteer have so many health`
            });
    }
    else if (req.body.defense <= 1 || req.body.defe >= 1) {
        res
            .status(400)
            .send({
                error: true,
                message: `Fighteer invalid defense`
            });
    }
    else if (typeof (req.body.health) !== "number") {
        res
            .status(400)
            .send({
                error: true,
                message: `Health must be Number`
            });
    }
    else if (typeof (req.body.defense) !== "number") {
        res
            .status(400)
            .send({
                error: true,
                message: `Defense must be Number`
            });
    }
    else if (typeof (req.body.power) !== "number") {
        res
            .status(400)
            .send({
                error: true,
                message: `Power must be Number`
            });
    }
    else if (req.body.id) {
        res
            .status(400)
            .send({
                error: true,
                message: `Id dont be used in request`
            });
    }
    else {
        removeExcessFields(req.body)
        next();
    }


    // TODO: Implement validatior for fighter entity during creation
}

const updateFighterValid = (req, res, next) => {
    if (req.body.health >= 100) {
        res
            .status(400)
            .send({
                error: true,
                message: `Fighteer have so many health`
            });
    }
    else if (req.body.defense <= 1 || req.body.defense >= 10) {
        res
            .status(400)
            .send({
                error: true,
                message: `Fighteer invalid defense`
            });
    }
    else if (typeof (req.body.health) !== "number") {
        res
            .status(400)
            .send({
                error: true,
                message: `Health must be Number`
            });
    }
    else if (typeof (req.body.defense) !== "number") {
        res
            .status(400)
            .send({
                error: true,
                message: `Defense must be Number`
            });
    }
    else if (typeof (req.body.power) !== "number") {
        res
            .status(400)
            .send({
                error: true,
                message: `Power must be Number`
            });
    }
    else if (req.body.id) {
        res
            .status(400)
            .send({
                error: true,
                message: `Id dont be used in request`
            });
    }
    else {
        removeExcessFields(req.body)
        next();
    }

    // TODO: Implement validatior for fighter entity during update
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;