const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', function (req, res, next) {
  res.body = FighterService.searchAll()
  res.error = `There are no fighters on server`
  next()
}, responseMiddleware)

router.get('/:id', function (req, res, next) {
  res.body = FighterService.search(req.params.id)
  res.error = `Fighter with id:${req.params.id} is not not found`
  next()
}, responseMiddleware)

router.post('/', createFighterValid, function (req, res, next) {
  res.body = FighterService.createFighter(req.body);
  res.error = `Fighter with name ${req.body.name} already exist`
  next()
}, responseMiddleware)

router.put('/:id', updateFighterValid, function (req, res, next) {
  res.body = FighterService.updateFighter(req.params.id, req.body)
  res.error = `Fighter with id ${req.params.id} is not found`
  next()
}, responseMiddleware)

router.delete('/:id', function (req, res, next) {
  res.body = FighterService.deleteFighter(req.params.id);
  res.error = `Fighter with id ${req.params.id} is not found`
  next()
}, responseMiddleware)


module.exports = router;