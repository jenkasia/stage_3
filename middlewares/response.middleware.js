const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    if (res.body) {
        res
            .status(200)
            .send(res.body);
    } else {
        res
            .status(404)
            .send({
                error: true,
                message: res.error
            });
    }
}

exports.responseMiddleware = responseMiddleware;