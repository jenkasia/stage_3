const { user } = require('../models/user');


const validateEmail = (inputText) => {
    return (/@gmail\.com$/.test(inputText))
}

const validatePhone = (phoneNumber) => {
    if (phoneNumber) {
        let phoneAfterCode = phoneNumber.slice(4)
        let phoneCode = phoneNumber.slice(0, 4)
        let isnum = (/^\d+$/.test(phoneAfterCode) && phoneAfterCode.length == 9 && phoneCode == "+380");
        return isnum
    }
    else {
        return 0
    }
}

const passValidator = (password) => {
    if (password) {
        const isPass = password.length >= 3;
        return isPass
    }
    else return 0

}


const removeExcessFields = (body) => {
    for (let key in body) {
        if (!user.hasOwnProperty(key)) {
            delete body[key]
        }
    }
}

const checkForAllFields = (body) => {
    for (let key in user) {
        if (!body.hasOwnProperty(key) && key !== 'id') {
            return false
        }
    }
    return true
}

const createUserValid = (req, res, next) => {

    removeExcessFields(req.body)
    const allRequiredFields = checkForAllFields(req.body);
    const isGmail = validateEmail(req.body.email);
    const isPhone = validatePhone(req.body.phoneNumber);
    const isPass = passValidator(req.body.password);
    const idExist = !!req.body.id

    if (!allRequiredFields) {
        res
            .status(400)
            .send({
                error: true,
                message: 'You are miss some fields in you request'
            })
    }

    else if (idExist) {
        res
            .status(400)
            .send({
                error: true,
                message: 'In reques exist ID, please remove it from body'
            })
    }
    else if (!isGmail || !isPhone || !isPass) {
        let message = `${!isGmail ? "Gmail, " : ""}${!isPhone ? "Phone, " : ""}${!isPass ? "Pass, " : ""}not valid`
        res
            .status(400)
            .send({
                error: true,
                message: message
            })
    } else {
        next()
    }
}

const updateUserValid = (req, res, next) => {

    removeExcessFields(req.body)
    const allRequiredFields = checkForAllFields(req.body);
    const isValidGmail = validateEmail(req.body.email);
    const isValidPhone = validatePhone(req.body.phoneNumber);
    const isValidPass = passValidator(req.body.password);
    const idExist = !!req.body.id

    if (!req.body) {
        res
            .status(400)
            .send({
                error: true,
                message: 'You are miss some fields in you request'
            })
    } else if (idExist) {
        res
            .status(400)
            .send({
                error: true,
                message: 'In reques exist ID, please remove it from body'
            })
    } else if (!isValidGmail || !isValidPhone || !isValidPass) {
        let message = `${!isValidGmail ? "Email, " : ""}${!isValidPhone ? "Phone, " : ""}${!isValidPass ? "Pass, " : ""}not valid`
        res
            .status(400)
            .send({
                error: true,
                message: message
            })
    }
    else {
        next()
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;